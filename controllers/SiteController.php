<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\Contacto;
use app\models\Productos;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContacto()
{
    $model = new \app\models\Contacto();

    if ($model->load(Yii::$app->request->post())) {
        if ($model->contact()) {
            
                return $this->render("mostrar");
        }
    }

    return $this->render('contacto', [
        'model' => $model,
    ]);
}

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
    
    public function actionOfertas()
    {
        $dataProvider = new ActiveDataProvider([
        'query' => Productos::find()->where(["oferta"=>1])]);
        return $this->render("ofertas",["dataProvider" => $dataProvider]);
    }
    
    public function actionProductos()
    {
        $dataProvider = new ActiveDataProvider([
        'query' => Productos::find()]);
        return $this->render("productos",["dataProvider" => $dataProvider]);
    }
    
    public function actionCategorias()
    {
        $resultado=Productos::find()->select("categoria")->distinct()->all();
        return $this->render("categorias",["resultado"=>$resultado]);
    }
    
    public function actionCategoria2($categoria)
    {
        $dataProvider = new ActiveDataProvider([
        'query' => Productos::find()->where(['categoria'=>$categoria])]);
        return $this->render("productos",["dataProvider" => $dataProvider]);
    }
    
    public function actionDondeestamos()
    {
        return $this->render('donde_estamos');
    }
    
    public function actionQuienessomos()
    {
        return $this->render('quienes_somos');
    }
    
    public function actionNuestrosproductos()
    {
        $fotos= Productos::find()
                ->select('foto')
                ->where(["oferta"=>1])
                ->asArray()
                ->all();
        
        $fotos= ArrayHelper::getColumn($fotos,"foto");
        $salida=[];
        foreach($fotos as $src)
        {
            $salida[]= Html::img("@web/img/$src");
        }
        return $this->render('nuestros_productos',["fotos"=>$salida]);
    }
    
    public function actionInformacion()
    {
        return $this->render('informacion');
    }
    
    public function actionVermas($id)
    {
        $salida=Productos::findOne($id);
        
        return $this->render('vermas',["model"=>$salida]);
    }
}
