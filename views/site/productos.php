<?php

use yii\grid\GridView;
use yii\bootstrap4\Html;

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        'id',
        'nombre',
        [
            'label'=>'Foto',
            'format'=>'raw',
            'value'=>function($data){
                $url='@web/img/' . $data->foto;
                return Html::img($url,['class'=>'img-fluid', 'style'=>'width:300px']);
            }
        ],
        [
            'label'=>'Precio',
            'value'=>function($data){
            return $data->precio . "€";
            }
        ],
        'oferta',
        'categoria',        
        
    ]]);

?>
