<?php
use yii\helpers\Html;
?>

<table>
    <tr><td rowspan="6"><?= Html::img('@web/img/' . $model->foto,['class'=>'img-fluid', 'style'=>'width:300px'])?></td><td><?= "Id: " . $model->id ?></td></tr>
    <tr><td><?= "Nombre: " . $model->nombre ?></td></tr>
    <tr><td><?= "Descripcion: " . substr($model->descripcion,0,5) . "..."?></td></tr>
    <tr><td><?= "Precio: " . $model->precio . "€"?></td></tr>
    <tr><td><?= "Oferta: " . $model->oferta ?></td></tr>
    <tr><td><?= "Categoria: " . $model->categoria  ?></td></tr>
    <tr><td><?= Html::a("Ver mas",["site/vermas", 'id' => $model->id], ['class' => 'btn btn-primary']) ?></td></tr>
    <hr>
    
</table>
