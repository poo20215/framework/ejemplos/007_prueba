<?php
use yii\widgets\DetailView;
use yii\helpers\Html;

echo DetailView::widget([
    'model' => $model,
    'attributes' => [
        'id',
        'nombre',
        'descripcion',
        'precio',
        'categoria',
        'oferta',
        [
            'label'=>'Foto',
            'format'=>'raw',
            'value'=>function($data){
                $url='@web/img/' . $data->foto;
                return Html::img($url,['class'=>'img-fluid', 'style'=>'width:300px']);
            }
        ],
    ],
]);