<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\assets\AppAsset;
use app\widgets\Alert;
use yii\bootstrap4\Breadcrumbs;
use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="d-flex flex-column h-100">
<?php $this->beginBody() ?>

<header>
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar navbar-expand-md navbar-dark bg-dark fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav'],
        'encodeLabels' => false, //para que funcionen los dibujos del menu
        'items' => [
            ['label' => '<i class="fas fa-house-user"></i> Inicio', 'url' => ['/site/index']],
            ['label' => '<i class="fa fa-check" aria-hidden="true"> </i>
            Ofertas', 'url' => ['/site/ofertas']],
            ['label' => '<i class="fas fa-cube"></i> Productos', 'url' => ['/site/productos']],
            ['label' => 'Categorias', 'url' => ['/site/categorias']],
            ['label' => 'Nosotros', 'items' =>[
                ['label' => 'Donde Estamos', 'url' => ['/site/dondeestamos']],
                ['label' => 'Quienes Somos', 'url' => ['/site/quienessomos']],
                ['label' => 'Nuestros productos', 'url' => ['/site/nuestrosproductos']],
                '<div class="dropdown-divider"></div>',
                ['label' => 'Informacion', 'url' => ['/site/informacion']]]],    
            ['label' => 'Contacto', 'url' => ['/site/contacto']],
        ],
    ]);
    NavBar::end();
    ?>
</header>

<main role="main" class="flex-shrink-0">
    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</main>

<footer class="footer mt-auto py-3 text-muted">
    <div class="container">
        <p class="float-left">&copy; My Company <?= date('Y') ?></p>
        <p class="float-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
